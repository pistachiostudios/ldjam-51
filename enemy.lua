local Enemy = class("Enemy")

function Enemy:initialize(x, y, color)
    -- in grid coordinates
    self.x = x
    self.y = y
    self.color = color
end

function Enemy:draw(offsetX, offsetY, opacity)
    local r = self.color[1]
    local g = self.color[2]
    local b = self.color[3]
    love.graphics.setColor(r, g, b, opacity)
    -- draw images.enemy, centered
    local scale = 0.9 * TileSize/60
    love.graphics.draw(images.enemy, offsetX + (self.x - 0.5) * TileSize, offsetY + (self.y - 0.5) * TileSize, 0, scale, scale, images.enemy:getWidth() / 2, images.enemy:getHeight() / 2)
end

return Enemy
