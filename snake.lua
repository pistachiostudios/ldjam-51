local Snake = class("Snake")

function Snake:initialize(x, y)
    -- in grid coordinates
    self.x = x
    self.y = y
    self.direction = "right"
    self.powerupRemaining = 0
    self.color = {0, 0.6, 0}

    self.body = {}
end

function Snake:draw(offsetX, offsetY, opacity)
    love.graphics.setColor(self.color[1], self.color[2], self.color[3], opacity)
    local size = TileSize*0.8

    -- draw body
    for i, segment in ipairs(self.body) do
        love.graphics.rectangle("fill", offsetX + (segment.x - 0.5) * TileSize - size/2, offsetY + (segment.y - 0.5) * TileSize - size/2, size, size)
    end

    -- draw head
    love.graphics.rectangle("fill", (self.x - 0.5) * TileSize - size/2 + offsetX, (self.y - 0.5) * TileSize + offsetY - size/2, size, size)

    -- draw line in direction of movement
    local lineLength = TileSize*0.5
    local lineX = (self.x - 0.5) * TileSize + offsetX
    local lineY = (self.y - 0.5) * TileSize + offsetY
    if self.direction == "right" then
        lineX2 = lineX + lineLength
        lineY2 = lineY
    elseif self.direction == "left" then
        lineX2 = lineX - lineLength
        lineY2 = lineY
    elseif self.direction == "up" then
        lineX2 = lineX
        lineY2 = lineY - lineLength
    elseif self.direction == "down" then
        lineX2 = lineX
        lineY2 = lineY + lineLength
    end
    love.graphics.setColor(0.6, 0, 0)
    love.graphics.setLineWidth(TileSize/8)
    love.graphics.line(lineX, lineY, lineX2, lineY2)
end

function Snake:move(dx, dy)
    local newX = self.x + dx
    local newY = self.y + dy
    if not self:occupies(newX, newY) then
        self.x = newX
        self.y = newY
        table.insert(self.body, {x = self.x, y = self.y})
    end
end

function Snake:occupies(x, y)
    if self.x == x and self.y == y then
        return true
    end
    for i, segment in ipairs(self.body) do
        if segment.x == x and segment.y == y then
            return true
        end
    end
    return false
end

return Snake
