-- a class for grids
-- and some functions for managing them

local Grid = class("Grid")

function Grid:initialize(config, level)
    -- size of the playing field (number of tiles)
    self.config = config
    self.width = config.width
    self.height = config.height
    self.thickness = config.thickness
    self.tiles = {}
    self.level = level

    local w = self.width
    local h = self.height
    local t = self.thickness -- of segments
    local sh = (h+t)/2 -- height of a vertical segment

    self.walls = {}

    self.type = nil
    if self.width > self.thickness then
        self.type = "digit"
    else
        self.type = "colon"
    end

    self.segmentMapping = {}
    if self.type == "digit" then
        self.segmentMapping = {
            A = {x=1, y=1, w=w, h=t, state = false},
            B = {x=w-t+1, y=1, w=t, h=sh, state = false},
            C = {x=w-t+1, y=sh-t+1, w=t, h=sh, state = false},
            D = {x=1, y=h-t+1, w=w, h=t, state = false},
            E = {x=1, y=sh-t+1, w=t, h=sh, state = false},
            F = {x=1, y=1, w=t, h=sh, state = false},
            G = {x=1, y=sh-t+1, w=w, h=t, state = false},
        }
    elseif self.type == "colon" then
        local dh = ((h+w)/2)-(2*w)
        self.segmentMapping = {
            A = {x=1, y=math.floor(w + dh/2 - w/2 + 1), w=w, h=w, state = true},
            B = {x=1, y=math.floor((w*2)+(dh*(3/2))-(w/2)+1), w=w, h=w, state = true},
        }
    end

    -- debug: segment sizes
    --for n, s in pairs(self.segmentMapping) do
    --    print("segment "..n.." size: "..s.w.."x"..s.h.." at "..s.x.."|"..s.y)
    --end

    for x = 1, self.width do
        self.tiles[x] = {}
        for y = 1, self.height do
            self.tiles[x][y] = nil
        end
    end

    -- set default digit
    if self.type == "digit" then
        self:setDigit(8)
    elseif self.type == "colon" then
        self:setSegment("A", true)
        self:setSegment("B", true)
    end

    self:setWalls() -- needs to be initialized after the digit
    
end

function Grid:isWalkable(x, y)
    if x < 1 or x > self.width or y < 1 or y > self.height then
        return false
    elseif self:isWall(x,y) then
        return false
    end
    return self.tiles[x][y]
end

function Grid:getRandomFreePosition()
    local x = math.random(1, self.width)
    local y = math.random(1, self.height)
    while not self:isWalkable(x, y) do
        x = math.random(1, self.width)
        y = math.random(1, self.height)
    end
    return x, y
end

function Grid:setDark()
    local segmentString = "ABCDEFG"
    for c in segmentString:gmatch"." do
        self:setSegment(c, false)
    end
end

function Grid:isWall(x, y)
    if self.config.obstacles > 0 then
        if self.walls[x][y] ~= false and self.walls[x][y] ~= nil then
            return true
        else
            return false
        end
    else
        return false
    end
end

function Grid:breakWall(x, y)
    if self:isWall(x, y) then
        self.walls[x][y] = self.walls[x][y] - 0.2001
        sounds.crunch:setPitch(1.2 + math.random() * 0.2)
        sounds.crunch:play()
    end
    if self.walls[x][y] <= 0 then
        self.walls[x][y] = false
    end
end

function Grid:testFlood(wx,wy)
    -- build test grid
    local floodGrid = {}
    for x = 1, self.width do
        floodGrid[x] = {}
        for y = 1, self.height do
            floodGrid[x][y] = nil
            if self.tiles[x][y] ~= nil then
                if self:isWall(x, y) == false then
                    floodGrid[x][y] = false
                end
            end
        end
    end
    -- set wall tile to be tested:
    floodGrid[wx][wy] = nil

    -- flooding
    local changed = nil
    -- start in top left corner (because there is no wall at the player's start position)
    floodGrid[1][1] = true
    while true do
        changed = 0
        for x = 1, self.width do
            for y = 1, self.height do
                if floodGrid[x][y] == false then
                    if (x < self.width and floodGrid[x+1][y] == true) or
                    (x > 1 and floodGrid[x-1][y] == true) or
                    (y < self.height and floodGrid[x][y+1] == true) or
                    (y > 1 and floodGrid[x][y-1] == true)
                    then
                        floodGrid[x][y] = true
                        changed = changed + 1
                    end
                end
            end
        end
        if changed == 0 then
            -- flooded everything that can be
            break
        end
    end
    -- check if there are unflooded tiles left:
    for x = 1, self.width do
        for y = 1, self.height do
            if floodGrid[x][y] == false then
                return false
            end
        end
    end
    -- if this is reached, everything can still be flooded with a wall tile at wx, wy
    return true

end

function Grid:setWalls()
    -- init walls grid with false:
    for x = 1, self.width do
        self.walls[x] = {}
        for y = 1, self.height do
            self.walls[x][y] = false
        end
    end
    -- set some walls to true, maybe:
    for i = 1, self.width * self.height do
        local x = math.random(1, self.width)
        local y = math.random(1, self.height)
        if (x > 1 or y > 1) and self.tiles[x][y] ~= nil then
            if (math.random() < self.config.obstacles ) and self:testFlood(x, y) then
                self.walls[x][y] = 1
            end
        end
    end
end

function Grid:setSegment(segmentLetter, state)
    local segment = self.segmentMapping[segmentLetter]
    self.segmentMapping[segmentLetter].state = state
    for x = segment.x, segment.x+segment.w-1 do
        for y = segment.y, segment.y+segment.h-1 do
            self.tiles[x][y] = state
        end
    end
end

function Grid:setColon(state)
    if self.type == "colon" then
        self:setSegment("A", state)
        self:setSegment("B", state)
    end
end

function Grid:setDigit(digit)
    if self.type == "digit" then
        local digitMapping = {
            [0] = "ABCDEF",
            [1] = "BC",
            [2] = "ABGED",
            [3] = "ABCDG",
            [4] = "BCFG",
            [5] = "ACDFG",
            [6] = "AFGCDE",
            [7] = "ABC",
            [8] = "ABCDEFG",
            [9] = "ABFGCD",
        }

        -- set all segments to false
        self:setDark()

        -- set segments
        local segmentString = digitMapping[digit]
        for c in segmentString:gmatch"." do
            self:setSegment(c, true)
        end
    end
end

function Grid:draw(offsetX, offsetY)

    local rx = TileSize * self.config.thickness / 8

    -- draw segments that are off:
    love.graphics.setColor(0.1,0.1,0.1)
    for _, segment in pairs(self.segmentMapping) do
        if segment.state == false then
            local x = offsetX + (segment.x - 1) * TileSize
            local y = offsetY + (segment.y - 1) * TileSize
            local w = segment.w * TileSize
            local h = segment.h * TileSize
            love.graphics.rectangle("fill", x, y, w, h, rx)
        end
    end
    
    -- draw segments that are on:
    love.graphics.setColor(unpack(self.config.segmentColor))
    for _, segment in pairs(self.segmentMapping) do
        if segment.state == true then
            local x = offsetX + (segment.x - 1) * TileSize
            local y = offsetY + (segment.y - 1) * TileSize
            local w = segment.w * TileSize
            local h = segment.h * TileSize
            love.graphics.rectangle("fill", x, y, w, h, rx)
        end
    end

    -- draw walls
    if self.config.obstacles > 0 then
        for x = 1, self.width do
            for y = 1, self.height do
                if self.walls[x][y] ~= false then
                    if self.walls[x][y] == 1 then
                        love.graphics.setColor(unpack(self.config.wallColor))
                    elseif self.walls[x][y] < 1 then
                        local v = self.walls[x][y]
                        local r = self.config.wallColor[1] * v
                        local g = self.config.wallColor[2] * v
                        local b = self.config.wallColor[3] * v
                        local a = self.config.wallColor[4] * v
                        love.graphics.setColor(r, g, b, a)
                    end
                    love.graphics.rectangle("fill", offsetX+(TileSize*(x-1)), offsetY+(TileSize*(y-1)), TileSize, TileSize, TileSize/8)
                end
            end
        end
    end
end

return Grid
