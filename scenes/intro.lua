local scene = {}

function scene:enter()
    videos.intro:play()
    music.intro:play()
end

function scene:leave()
    music.intro:stop()
end

function scene:draw()
    love.graphics.draw(videos.intro, 0, 0)
end

function scene:update()
    if not videos.intro:isPlaying() then
        roomy:enter(scenes.title)
    end
end

function scene:handleInput()
    if input:isPressed("click") then
        roomy:enter(scenes.title)
    end
end

return scene
