local scene = {}

function scene:enter()
    if not backgroundMusic:isPlaying() and not backgroundMusicMuted then
        backgroundMusic:play()
    end
end

function scene:draw()
    love.graphics.draw(images.background2, 0, 0)
    love.graphics.setColor(0, 0, 0, 0.6)
    love.graphics.rectangle("fill", 0, 0, CANVAS_WIDTH, CANVAS_HEIGHT)

    love.graphics.setColor(1, 1, 1) --white
    love.graphics.setFont(fonts.default)
    love.graphics.setColor(0.5, 0.5, 0.5)
    love.graphics.printf("Pistachio Studios presents:", 0, CANVAS_HEIGHT*0.05, CANVAS_WIDTH, "center")
    love.graphics.setFont(fonts.title)
    love.graphics.setColor(0.8, 0.5, 0.1)
    love.graphics.printf("Alarm Clock", 0, CANVAS_HEIGHT*0.16, CANVAS_WIDTH, "center")
    if os.time() % 2 == 0 then
        love.graphics.printf(":", CANVAS_WIDTH/100, CANVAS_HEIGHT*0.16, CANVAS_WIDTH, "center")
    end
    love.graphics.printf("Arcade", 0, CANVAS_HEIGHT*0.41, CANVAS_WIDTH, "center")
    love.graphics.setColor(0.5, 0.5, 0.5)
    love.graphics.setFont(fonts.small)
    love.graphics.printf("made in 72 hours for Ludum Dare 51", 0, CANVAS_HEIGHT*0.68, CANVAS_WIDTH, "center")
    love.graphics.printf("by flauschzelle and blinry", 0, CANVAS_HEIGHT*0.75, CANVAS_WIDTH, "center")
    love.graphics.setFont(fonts.default)

    if os.time() % 2 == 0 then
        love.graphics.setColor(1, 1, 1)
        love.graphics.printf("Press "..Input:getKeyString("click").." to start!", 0, CANVAS_HEIGHT*0.87, CANVAS_WIDTH, "center")
    end
end

function scene:handleInput()
    if input:isPressed("click") then
        
        -- always go to level 1
        LevelManager:currentLevel():reset()
        LevelManager:currentLevel().started = false
        LevelManager.current = 1

        roomy:enter(scenes.gameplay)
    end
    if input:isPressed("quit") then
        love.window.setFullscreen(false)
        love.event.quit()
    end
end

return scene
