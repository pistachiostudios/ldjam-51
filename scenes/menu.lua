local scene = {} 

local MenuMarginY = 60
local MenuMarginX = 240
local LINES = 5

Arrow = {} --initialize geometry object with draw function
function Arrow:draw(x, y)
    local size = 30
    local vertices = {x-size, y, x, y+size, x-size, y+(2*size)}
    love.graphics.setColor(0.8, 0.5, 0.1)
    love.graphics.polygon("fill", vertices)
    love.graphics.setColor(1,1,1)
end

function scene:draw_level_list()
    local lvls = LevelManager.level_count
    local colWidth = (CANVAS_WIDTH - 2*MenuMarginX)/(math.ceil(lvls/LINES))
    local lineHeight = (CANVAS_HEIGHT - 6*MenuMarginY)/LINES
    local i = 1
    while i <= lvls do
        local number = i..". "
        if i <= 9 then
            number = "0"..i..". "
        end
        local name = number..Levels[i].name
        local posX = MenuMarginX + ((math.ceil(i/LINES)-1)*colWidth)
        local posY = 5*MenuMarginY + (((i-1)%LINES) * lineHeight)


        love.graphics.setColor(1,1,1)

        love.graphics.printf(name, posX, posY, colWidth, "left")
        -- draw selection indicator:
        if i == self.selected then
            Arrow:draw(posX - 30, posY - 10)
        end
        i = i +1
    end
end

function scene:draw()
    love.graphics.setColor(0.8, 0.5, 0.1)
    love.graphics.setFont(fonts.default)
    love.graphics.printf("Alarm Clock Arcade", 0, MenuMarginY, CANVAS_WIDTH, "center")

    love.graphics.setFont(fonts.small)
    love.graphics.setColor(1, 1, 1) --white
    self:draw_level_list()

    love.graphics.setFont(fonts.tiny)
    love.graphics.setColor(0.8, 0.5, 0.1)
    love.graphics.printf("Press "..string.upper(Input:getKeyString("menu")).." to return to the game or choose a level and press "..Input:getKeyString("click").." to switch levels.", 0, CANVAS_HEIGHT - 180, CANVAS_WIDTH, "center")
    love.graphics.setColor(1, 1, 1) --white
    love.graphics.printf("Edit '" .. configFilePath .. "' to configure the input mapping.", 0, CANVAS_HEIGHT-90, CANVAS_WIDTH, "center")

end

function scene:update(dt)
    -- nothing to update here
end

function scene:handleInput()
    if input:isPressed("quit") then
        roomy:enter(scenes.title)
    end

    if input:isPressed("menu") then
        roomy:pop() -- leave menu
    end

    if input:isPressed("click") then
        local newlevelno = self.selected
        if newlevelno <= LevelManager.level_count then
            -- reset current level state
            if LevelManager.current ~= newlevelno then
                LevelManager:currentLevel():reset()
                if LevelManager:currentLevel().intro ~= nil then
                    LevelManager:currentLevel().started = false
                end
                -- switch to new level
                LevelManager.current = newlevelno
                LevelManager:currentLevel():reset() --reset level state
            end
            -- leave menu
            roomy:pop()
        end

    end

    if input:isPressed("up") and self.selected > 1 then
        self.selected = self.selected - 1
    end
    if input:isPressed("down") and self.selected < LevelManager.level_count then
        self.selected = self.selected + 1
    end

end

function scene:enter()
    self.selected = LevelManager.current
end

return scene
