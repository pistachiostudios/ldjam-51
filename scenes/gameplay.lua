local scene = {}

function scene:enter()
    self.playerMovementCooldownMax = 0.15 -- in seconds
    self.playerMovementCooldownRemaining = 0
end

--function scene:pause()
--    self.musicPosition = self.music:tell()
--    self.music:pause()
--end
--
--function scene:leave()
--    self.music:stop()
--end

--function scene:resume()
--    self.music:stop()
--    self.music:play()
--    print ("Music position: " .. self.musicPosition)
--    self.music:seek(self.musicPosition)
--end

function scene:update(dt)
    LevelManager.currentLevel():update(dt)
    if self.playerMovementCooldownRemaining > 0 then
        if LevelManager.currentLevel().player.powerupRemaining > 0 then
            self.playerMovementCooldownRemaining = self.playerMovementCooldownRemaining - 2*dt
        else
            self.playerMovementCooldownRemaining = self.playerMovementCooldownRemaining - dt
        end
    end

    local howOftenToSnore = 2*60 -- in seconds
    if math.random() < 1/(howOftenToSnore*60) then
        sounds.snore:play()
    end
end

function scene:draw()
    love.graphics.draw(images.background, 0, 0)

    -- draw clock frame

    local clockFramePadding = 2 * MarginSize

    local thickness = LevelManager.currentLevel().config.thickness
    local blockSize = TileSize * thickness
    local margin = MarginSize
    local x = -5 * blockSize
    local y = margin
    local w = (2 * LevelManager.currentLevel().config.width + (9 * thickness) ) * TileSize + clockFramePadding
    local h = ((LevelManager.currentLevel().config.height) * TileSize) + (2 * clockFramePadding)
    local rx = margin

    -- drop shadow
    love.graphics.setColor(0, 0, 0)
    love.graphics.rectangle ("fill", x - margin/4, y - margin/4, w + margin/2, h + margin/2, rx*1.2)

    -- frame
    love.graphics.setColor(0.4, 0.4, 0.4)
    love.graphics.rectangle ("fill", x, y, w, h, rx)

    -- dark inside
    love.graphics.setColor(0, 0, 0)
    love.graphics.rectangle("fill", x+(margin/2), y+(margin/2), w-margin, h-margin, rx*0.8)
    
    -- draw level

    love.graphics.setColor(1, 1, 1) --white

    LevelManager.currentLevel():draw()
end

function scene:handleInput()
    if input:isPressed("click") then
        --print("click in gameplay")
        -- if LevelManager.currentLevel().started then
        --     print("started!")
        -- else
        --     print("not started!")
        -- end
        if LevelManager.currentLevel().started == false then
            -- start level from intro
            LevelManager.currentLevel():start()
        elseif LevelManager.currentLevel():isWon() then
            if nextLevel() == false then    -- goes to next level if there is one
                roomy:enter(scenes.credits) -- show credits screen
            end
        elseif LevelManager.currentLevel():isLost() then
            -- restart level
            LevelManager.currentLevel():reset()
            LevelManager.currentLevel():start()
        else
        -- gameplay stuff
            sounds.meow:setPitch(0.5+math.random())
            --sounds.meow:play()
        end
    end

    if LevelManager.currentLevel().started 
    and not LevelManager.currentLevel():isWon() 
    and not LevelManager.currentLevel():isLost() 
    then
        -- movement controls
        local directionKeys = {"up", "down", "left", "right"}
        local directionMapping = {
            up = {x=0, y=-1},
            down = {x=0, y =1},
            left = {x=-1, y=0},
            right = {x=1, y=0},
        }
        for _, dk in ipairs(directionKeys) do
            if input:isPressed(dk) or input:isDown(dk) then
                if self.playerMovementCooldownRemaining <= 0 then
                    if LevelManager.currentLevel().config.playerCharacter == "pacman" then
                        LevelManager.currentLevel():movePlayer(directionMapping[dk].x, directionMapping[dk].y)
                    end
                    LevelManager.currentLevel().player.direction = dk
                    self.playerMovementCooldownRemaining = self.playerMovementCooldownMax
                end
            end
        end

        if LevelManager.currentLevel().config.playerCharacter == "snake" then
            if self.playerMovementCooldownRemaining <= 0 then
                local player = LevelManager.currentLevel().player
                LevelManager.currentLevel():movePlayer(directionMapping[player.direction].x, directionMapping[player.direction].y)
                self.playerMovementCooldownRemaining = self.playerMovementCooldownMax
            end
        end
    end

    if input:isPressed("pause") then
        roomy:push(scenes.pause)
    end

    if input:isPressed("menu") then
        roomy:push(scenes.menu)
    end

    -- cheat to win current level
    if input:isPressed("cheat") then
        LevelManager.currentLevel().won = true
    end

    if input:isPressed("quit") then
        roomy:push(scenes.title)
    end

    if input:isPressed("mute") then
        backgroundMusicMuted = not backgroundMusicMuted
        if backgroundMusicMuted then
            backgroundMusic:pause()
        else
            backgroundMusic:play()
        end
    end
end

return scene
