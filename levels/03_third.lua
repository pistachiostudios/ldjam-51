local name = "Think Big"
local config = {
    thickness = 3,
    enemies = 3,
    obstacles = 0.5,
    segmentColor = {0, 0.7, 0.8, 1}, -- cyan
    dotColor = {0, 0.3, 0.4, 1}, -- dark cyan
    wallColor = {0, 0, 0.4, 1}, -- dark blue
    playerColor = {1, 0.7, 0, 1}, -- orangey yellow
    enemyColor = {1, 0.3, 0, 1}, -- red-orange
    preOutro = "This should be enough, right?"
}
local content = ""
local intro = "Maybe if we keep increasing the resolution of the display, the alarm will be more convincing?!"
local outro = "Nope. Your human snoozed again. Why would they do that. Why would anyone ever do that?"

local level = Level:new(name, config, content, intro, outro)

return level
