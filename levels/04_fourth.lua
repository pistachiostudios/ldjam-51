local name = "Last Chance"
local config = {
    thickness = 4,
    segmentWidth = 8,
    segmentHeight = 6,
    enemies = 5,
    obstacles = 0.7,
    startTime = 30,
    segmentColor = {0.8, 0, 1, 1}, -- purple
    dotColor = {0.4, 0, 0.6, 1}, -- dark purple
    wallColor = {0.2, 0, 0.4, 1}, -- another dark purple
    playerColor = {0, 0.7, 0, 1}, -- lime green
    enemyColor = {0.5, 1, 0, 1}, -- greenish yellow
    preOutro = "Phew... "
}
local intro = "This is your last chance! Quickly, your human is already late for work!"
local outro = "Turns out: It was just a dream. There's no such things as as monsters inside an alarm clock. You still have plenty of time. :)"
local content = ""

local level = Level:new(name, config, content, intro, outro)

return level
