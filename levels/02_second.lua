local name = "Nightmares"
local config = {
    thickness = 2,
    segmentWidth = 5,
    segmentHeight = 5,
    obstacles = 0.2,
    enemies = 2,
    segmentColor = {0, 0.7, 0, 1}, -- lime green
    dotColor = {0, 0.3, 0, 1}, -- dark green
    wallColor = {0, 0.2, 0, 1}, -- another dark green
    playerColor = {0.3, 0, 0.4, 1}, -- dark purple
    enemyColor = {1, 0, 1, 1}, -- pink
    preOutro = "Yay, you made it!"
}
local content = ""
local intro = "Terrible monsters appeared in your clock! Can you do anything against them?"
local outro = "... oh. The human snoozed again. But those dark blocks are quite tasty, aren't they?"

local level = Level:new(name, config, content, intro, outro)

return level
