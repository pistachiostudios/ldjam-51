local name = "Wake up call!"
local intro = "Eat all dots to wake up your human! (Press "..Input:getKeyString("click").." to start)"
local outro = "Unfortunately, the human used snooze! It was very effective!"
local config = {
    preOutro = "Good job!",
    thickness = 1,
    segmentWidth = 3,
    segmentHeight = 3,
    obstacles = 0,
    enemies = 0,
    startTime = 20,
    segmentColor = {0.8, 0.5, 0, 1}, -- light orange
    dotColor = {0.2, 0.1, 0, 1}, -- dark brown
    wallColor = {0.1, 0.05, 0, 1}, -- another dark brown
    playerColor = {0, 0.4, 0.6, 1} -- dark cyan
}
local content = ""

local level = Level:new(name, config, content, intro, outro)

return level
