local Powerup = class("Powerup")

function Powerup:initialize(x, y)
    -- in grid coordinates
    self.x = x
    self.y = y
    self.color = {
        r = math.random(),
        g = math.random(),
        b = math.random(),
    }
    self.colorOffset = math.random(200)
end

function Powerup:update(dt)
    self.colorOffset = self.colorOffset + 1
    local newColor = ColorCycle(self.colorOffset)
    self.color.r = newColor[1]
    self.color.g = newColor[2]
    self.color.b = newColor[3]
end

function Powerup:draw(offsetX, offsetY, opacity)
    love.graphics.setColor(self.color.r, self.color.g, self.color.b, opacity)
    love.graphics.circle("fill", offsetX + (self.x - 0.5) * TileSize, offsetY + (self.y - 0.5) * TileSize, TileSize * 0.4, 7)
end

return Powerup
