local Player = class("Player")

function Player:initialize(x, y, color)
    -- in grid coordinates
    self.x = x
    self.y = y
    self.direction = "right"
    self.powerupRemaining = 0
    self.color = color
    self.maxMouthAngle = math.pi / 4 -- half the width of the mouth
    self.mouthAngle = self.maxMouthAngle
end

function Player:update(dt)
    if self.powerupRemaining - dt < 1 and self.powerupRemaining > 1 then
        sounds.powerdown:setPitch(0.8 + 0.4 * math.random())
        sounds.powerdown:play()
    end
    -- count down powerup
    self.powerupRemaining = self.powerupRemaining - dt
    -- animate the width of the mouth
    self.mouthAngle = self.maxMouthAngle * (math.sin((love.timer.getTime()/2 + 0.5)*50))
end

function Player:draw(offsetX, offsetY, opacity)

    local posX = offsetX + (self.x - 0.5) * TileSize
    local posY = offsetY + (self.y - 0.5) * TileSize
    local normalRadius = TileSize * 0.35
    local powerRadius = TileSize * 0.45
    local smallRadius = TileSize * 0.25
    local radius = normalRadius
    local mouthAngle = self.mouthAngle
    local angle1 = mouthAngle
    local angle2 = (math.pi * 2) - mouthAngle

    if self.direction == "right" then
        angle1 = mouthAngle
        angle2 = (math.pi * 2) - angle1
    elseif self.direction == "down" then
        angle1 = (math.pi / 2) + mouthAngle
        angle2 = (math.pi * 5/2) - mouthAngle
    elseif self.direction == "left" then
        angle1 = math.pi + mouthAngle
        angle2 = (math.pi * 3) - mouthAngle
    elseif self.direction == "up" then
        angle1 = (math.pi * 3/2) + mouthAngle
        angle2 = (math.pi * 7/2) - mouthAngle
    end

    local powerColorR1 = math.max(self.color[1] - 0.1, 0)
    local powerColorG1 = math.max(self.color[2] - 0.1, 0)
    local powerColorB1 = math.max(self.color[3] - 0.1, 0)
    local powerColorR2 = math.min(self.color[1] + 0.1, 1)
    local powerColorG2 = math.min(self.color[2] + 0.1, 1)
    local powerColorB2 = math.min(self.color[3] + 0.1, 1)
    local powerColor1 = {powerColorR1, powerColorG1, powerColorB1, opacity}
    local powerColor2 = {powerColorR2, powerColorG2, powerColorB2, opacity}

    if self.powerupRemaining > 0 and math.floor((self.powerupRemaining*10))%2 == 0 then
        love.graphics.setColor(unpack(powerColor1))
        love.graphics.arc("fill", posX, posY, powerRadius, angle1, angle2)
        love.graphics.setColor(unpack(powerColor2))
        radius = smallRadius
    elseif self.powerupRemaining > 0 and math.floor((self.powerupRemaining*10))%2 ~= 0 then
        love.graphics.setColor(unpack(powerColor2))
        love.graphics.arc("fill", posX, posY, powerRadius, angle1, angle2)
        love.graphics.setColor(unpack(powerColor1))
        radius = smallRadius
    else
        love.graphics.setColor(self.color[1], self.color[2], self.color[3], opacity)
        radius = normalRadius
    end

    love.graphics.arc("fill", posX, posY, radius, angle1, angle2)

end

return Player
