require "lib.slam"
require "lib.helpers"
local moonshine = require 'lib.moonshine'

vector = require "lib.hump.vector"
tlfres = require "lib.tlfres"
class = require 'lib.middleclass' -- see https://github.com/kikito/middleclass
libroomy = require 'lib.roomy' -- see https://github.com/tesselode/roomy
Input = require "lib.input.Input"
Cursor = require "cursor"
Level = require "level"
inspect = require('lib.inspect')

CANVAS_WIDTH = 1920
CANVAS_HEIGHT = 1080
MarginSize = CANVAS_HEIGHT/20
TileSize = 40

roomy = libroomy.new() -- roomy is the manager for all the rooms (scenes) in our game
scenes = {} -- initialize list of scenes

function love.load()
    -- initialize randomness in two ways:
    love.math.setRandomSeed(os.time())
    math.randomseed(os.time())

    -- set up default drawing options
    love.graphics.setBackgroundColor(0.05, 0.0, 0.05)

    -- load assets
    images = {}
    for file in listUsefulFiles("images") do
        images[file.basename] = love.graphics.newImage(file.path)
    end

    sounds = {}
    for file in listUsefulFiles("sounds") do
        sounds[file.basename] = love.audio.newSource(file.path, "static")
        sounds[file.basename]:setVolume(0.8)
    end
    sounds.pop:setVolume(0.2)
    sounds.tick:setVolume(0.2)
    sounds.tock:setVolume(0.2)

    music = {}
    for file in listUsefulFiles("music") do
        music[file.basename] = love.audio.newSource(file.path, "stream")
        music[file.basename]:setLooping(true)
    end
    backgroundMusic = music["pinball_spring"]
    backgroundMusic:setPitch(120.0/160.0)
    backgroundMusic:setVolume(0.1)
    backgroundMusicMuted = false

    fonts = {}
    for file in listUsefulFiles("fonts") do
        fonts[file.basename] = {}
        local sizes = {30, 60, 80, 300}
        for _, fontsize in ipairs(sizes) do
            fonts[file.basename][fontsize] = love.graphics.newFont(file.path, fontsize)
        end
    end
    fonts.title = fonts["technology_bold"][300]
    fonts.default = fonts["aldrich"][80]
    fonts.small = fonts["aldrich"][60]
    fonts.tiny = fonts["aldrich"][30]
    love.graphics.setFont(fonts.default)

    videos = {}
    for file in listUsefulFiles("videos") do
        videos[file.basename] = love.graphics.newVideo(file.path, {audio=false})
    end

    -- levels
    basenames = {}
    for file in listUsefulFiles("levels") do
        table.insert(basenames, file.basename)
    end
    table.sort(basenames) --sort level filenames in alphabetical order
    for i, level in ipairs(basenames) do
        Levels[i] = require ("levels." .. level)
    end

    InitLevelManager() -- this must be done after the levels are loaded from the filesystem

    -- scenes
    for file in listUsefulFiles("scenes") do
        scenes[file.basename] = require ("scenes." .. file.basename)
    end
    roomy:hook({exclude = {"draw", "update"}}) --hook roomy in to manage the scenes (with exceptions)
    roomy:enter(scenes.intro)

    input = Input:new()

    -- set up effects
    effect = moonshine(moonshine.effects.scanlines).chain(moonshine.effects.colorgradesimple).chain(moonshine.effects.glow).chain(moonshine.effects.godsray)
    effect.godsray.exposure = 0.2
    effect.godsray.weight = 0.2
    effect.godsray.samples = 5
    effect.godsray.density = 0.02
    --effect.glow.min_luma = 0.1
    effect.glow.strength = 20
    effect.scanlines.width = 2
    effect.scanlines.thickness = 1
    effect.scanlines.opacity = 0.15

    --effect.disable("scanlines")
    effect.disable("colorgradesimple")
    effect.disable("glow")
    --effect.disable("godsray")
end

function love.update(dt)
    input:update(dt)
    handleInput()
    roomy:emit("handleInput")
    roomy:emit("update", dt)
end

function love.mousemoved(x, y, dx, dy, istouch)
    input:mouseMoved(dx, dy)
end

-- not sure if we need this, only makes sense with absolute mouse positions
function love.mouse.getPosition()
    return tlfres.getMousePosition(CANVAS_WIDTH, CANVAS_HEIGHT)
end

-- helper function for things that change color
function ColorCycle(i)
    local speed = 0.05
    local TAU = 2*math.pi

    return {
        0.5+0.5*math.cos(i*speed + 0),
        0.5+0.5*math.cos(i*speed + TAU/3),
        0.5+0.5*math.cos(i*speed + TAU*2/3),
    }
end

-- helper function for dimming the level behind when text is shown
function DimRect()
    love.graphics.setColor(0, 0, 0, 0.5)
    love.graphics.rectangle("fill", 0, 0, CANVAS_WIDTH, CANVAS_HEIGHT)
    love.graphics.setColor(1, 1, 1)
end


function handleInput()
    if input:isPressed("fullscreen") then
        isFullscreen = love.window.getFullscreen()
        love.window.setFullscreen(not isFullscreen)
    end
end

function love.draw()
    love.graphics.setColor(1, 1, 1) -- white

    effect(function()
        tlfres.beginRendering(CANVAS_WIDTH, CANVAS_HEIGHT)

        -- (draw any global stuff here)
        -- Show this somewhere to the user so they know where to configure
        --love.graphics.printf("Edit '" .. configFilePath .. "' to configure the input mapping.", 0, 990, CANVAS_WIDTH, "center")

        -- draw scene-specific stuff:
        roomy:emit("draw")

        tlfres.endRendering()
    end)
end

function love.resize(w, h)
    effect.resize(w, h)
end
