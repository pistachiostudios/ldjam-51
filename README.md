Sound effects:

- https://freesound.org/people/FastForwardSoundEffects/sounds/262448/
- https://freesound.org/people/straget/sounds/408741/
- https://freesound.org/people/ZyryTSounds/sounds/219244/
- https://freesound.org/people/ProjectsU012/sounds/333785/
- https://freesound.org/people/cabled_mess/sounds/350987/
- https://freesound.org/people/Kenneth_Cooney/sounds/609336/
- https://freesound.org/people/neopolitansixth/sounds/579628/
- https://freesound.org/people/se2001/sounds/470768/
- https://freesound.org/people/straget/sounds/405423/

Music:

- "Decline" by Kevin MacLeod
- "Pinball Spring" by Kevin MacLeod
