-- a class for levels 
-- and some functions for managing them

Grid = require "grid"
Player = require "player"
Snake = require "snake"
Enemy = require "enemy"
Powerup = require "powerup"

local Level = class("Level")

function Level:initialize(name, config, content, intro, outro, lostMessage)
    self.name = name
    self.config = config
    self.content = content

    self.started = false
    self.won = false
    self.lost = false

    -- set default values
    self.config.segmentWidth = config.segmentWidth or 5
    self.config.segmentHeight = config.segmentHeight or 7
    self.config.height = self.config.segmentHeight*2 + self.config.thickness*3
    self.config.width = self.config.segmentWidth + self.config.thickness*2
    self.config.thickness = config.thickness or 2
    self.config.enemies = config.enemies or 1
    self.config.obstacles = config.obstacles or 0
    self.config.playerCharacter = config.playerCharacter or "pacman"

    if config.obstacles == nil then
        self.config.obstacles = true
    else
        self.config.obstacles = config.obstacles
    end
    if config.segmentColor == nil then
        self.config.segmentColor = {0, 0.7, 0.8, 1}
    else
        self.config.segmentColor = config.segmentColor
    end
    if config.wallColor == nil then
        self.config.wallColor = {0, 0, 0.2, 1}
    else
        self.config.wallColor = config.wallColor
    end
    if config.dotColor == nil then
        self.config.dotColor = {0, 0.1, 0.3, 1}
    else
        self.config.dotColor = config.dotColor
    end

    
    local gameWidthInTiles = 3 * self.config.thickness + 2 * self.config.width
    local gameHeightInTiles = self.config.height

    self.TileSize = math.min(
        (CANVAS_WIDTH - (3 * MarginSize))/gameWidthInTiles,
        (CANVAS_HEIGHT - (6 * MarginSize))/gameHeightInTiles
    )

    if config.startTime == nil then
        self.config.startTime = 0
    else
        self.config.startTime = config.startTime
    end

    -- clock  grids
    local gridHeight = self.config.height
    local gridThickness = self.config.thickness
    self.gridTen = Grid:new(self.config)
    self.gridTen:setDigit(self.config.startTime/10)

    self.gridOne = Grid:new(self.config)
    self.gridOne:setDigit(self.config.startTime%10)

    self.gridColon = Grid:new({width = gridThickness, height = gridHeight, thickness = gridThickness, segmentColor = self.config.segmentColor, wallColor = self.config.wallColor, obstacles = 0})
    self.gridColon:setColon(true)

    -- time counters
    self.seconds = self.config.startTime%10
    self.tenSeconds = self.config.startTime/10

    -- init collectible objects
    self.objects = {}
    self.powerups = {}

    -- nil is acceptable as a default value here!
    self.intro = intro
    -- start immediately if the level has no intro
    if self.intro == nil then
        self:start()
    end

    -- nil is acceptable as a default value here!
    self.preOutro = config.preOutro
    self.preWon = false

    -- default outro text
    if outro ~= nil then
        self.outro = outro
    else
        self.outro = "Congratulations, you won! Press "..Input:getKeyString("click").." to continue."
    end

    -- default lost text
    if lostMessage ~= nil then
        self.lostMessage = lostMessage
    else
        self.lostMessage = "Oh no! Press "..Input:getKeyString("click").." to restart."
    end

    if config.playerColor == nil then
        self.config.playerColor = {1, 0.9, 0, 1} -- default: yellow
    else
        self.config.playerColor = config.playerColor
    end

    if config.enemyColor == nil then
        self.config.enemyColor = {1, 0, 0, 1} -- default: red
    else
        self.config.enemyColor = config.enemyColor
    end

    self.preWonTime = 2.5
    self.preWonTimer = self.preWonTime
    self.moveDiscarder = 0

    self:reset()
end

function Level:fillWithObjects()
    for x = 1, self.gridTen.width do
        self.objects[x] = {}
        for y = 1, self.gridTen.height do
            if self.gridTen.tiles[x][y] ~= nil and not self.gridTen:isWall(x, y) then
                self.objects[x][y] = true
                --print("created object at x="..x.." , y="..y)
            end
        end
    end
end

function Level:countObjects()
    local object_count = 0
    for x = 1, self.gridTen.width do
        for y = 1, self.gridTen.height do
            if self.objects[x][y] == true then
                object_count = object_count + 1
            end
        end
    end
    return object_count
end

function Level:update(dt)
    if not (self.lost or self.won or self.preWon or not self.started) then
        local second_changed = false
        local old_seconds = self.seconds
        self.seconds = self.seconds+dt
        if math.floor(self.seconds) > math.floor(old_seconds) then
            if self.seconds % 2 == 0 then
                --sounds.tick:setPitch(0.9+0.2*math.random())
                --sounds.tick:setVolume(0.1+0.008*self.seconds*self.seconds)
                sounds.tick:play()
            else
                --sounds.tock:setPitch(0.9+0.2*math.random())
                --sounds.tock:setVolume(0.1+0.008*self.seconds*self.seconds)
                sounds.tock:play()
            end
            second_changed = true
            if math.floor(self.seconds) >= 10 then
                self.seconds = self.seconds-10
                self.tenSeconds = self.tenSeconds+1
                sounds.blip:setPitch(0.6+math.random()*0.2)
                sounds.blip:play()
                if self.tenSeconds >= 6 then
                    self.tenSeconds = 0
                end
                self.gridTen:setDigit(self.tenSeconds)
                if not self.gridTen:isWalkable(self.player.x, self.player.y) then
                    -- oh no, it suddenly got dark around the player
                    self.lost = true
                    sounds.die:play()
                end

            end
            self.gridOne:setDigit(math.floor(self.seconds))
        end
        if second_changed and math.floor(self.seconds)%2 == 0 then
            self.gridColon:setColon(true)
        elseif second_changed then
            self.gridColon:setColon(false)
        end

        -- update player
        self.player:update(dt)

        -- try to pick up object
        self:pickupObject(self.player.x, self.player.y)

        -- move enemies
        for i = 1, #self.enemies do
            local enemy = self.enemies[i]
            local x, y = enemy.x, enemy.y
            local dx, dy = 0, 0
            if math.random() < 0.05 then
                if math.random() < 0.5 then
                    dx = math.random(-1, 1)
                else
                    dy = math.random(-1, 1)
                end
            end
            if self.gridTen:isWalkable(x + dx, y + dy) then
                enemy.x = x + dx
                enemy.y = y + dy
            end
        end

        -- check for enemy-player collisions
        local enemiesToRemove = {}
        for i = 1, #self.enemies do
            local enemy = self.enemies[i]
            if enemy.x == self.player.x and enemy.y == self.player.y then
                if self.player.powerupRemaining > 0 then
                    table.insert(enemiesToRemove, i)
                    sounds.nom:setPitch(0.8 + 0.4 * math.random())
                    sounds.nom:play()
                else
                    self.lost = true
                    sounds.die:play()
                end
            end
        end
        for i = #enemiesToRemove, 1, -1 do
            table.remove(self.enemies, enemiesToRemove[i])
        end

        -- check for powerup collisions
        local powerupsToRemove = {}
        for i = 1, #self.powerups do
            local powerup = self.powerups[i]
            if powerup.x == self.player.x and powerup.y == self.player.y then
                self.player.powerupRemaining = 6
                sounds.powerup:setPitch(0.8 + 0.4 * math.random())
                sounds.powerup:play()
                table.insert(powerupsToRemove, i)
            end
        end
        for i = 1, #powerupsToRemove do
            table.remove(self.powerups, powerupsToRemove[i])
        end

        -- spawn powerup sometimes
        if math.random() < 0.001 then
            self:spawnPowerup()
        end

        -- update powerups (color)
        for _, p in pairs(self.powerups) do
            p:update(dt)
        end
    elseif self.preWon then
        self.preWonTimer = self.preWonTimer - dt
        if self.preWonTimer <= 0 then
            self.won = true
        end
    end
end

function Level:movePlayer(dx, dy)
    -- Check if the player can move away from the current position.
    if not self.gridTen:isWalkable(self.player.x, self.player.y) then
        return
    end

    -- If target position is walkable, move the player there.
    local newX = self.player.x+dx
    local newY = self.player.y+dy
    if self.gridTen:isWalkable(newX, newY) then
        if self.config.playerCharacter == "pacman" then
            self.player.x = newX
            self.player.y = newY
        else
            self.player:move(dx, dy)
        end
    else
        if newX <= self.config.width and newX > 0 and newY <= self.config.height and newY > 0 then
            if self.gridTen:isWall(newX,newY) then
                if self.moveDiscarder > 0 then
                    self.moveDiscarder = self.moveDiscarder - 1
                else
                    -- damage wall when running against it
                    self.gridTen:breakWall(newX, newY)
                    self.moveDiscarder = 1
                end
            end
        end
    end
end

function Level:pickupObject(x, y)
    if self.objects[x][y] == true then
        sounds.pop:setPitch(0.5+math.random())
        sounds.pop:play()

        -- remove object from the map and counter
        self.objects[x][y] = false

        self.remaining_objects = self.remaining_objects -1
        -- check win condition
        if self.remaining_objects == 0 then
            self:win()
        end
    end
end

function Level:draw()
    TileSize = self.TileSize
    local gridTenOffsetX = self.config.thickness * TileSize * 3
    local gridTenOffsetY = 3 * MarginSize

    -- draw grid
    local colonOffset = 0
    if self.config.thickness % 2 == 0 then
        if self.config.height / self.config.thickness % 2 == 0 then
            colonOffset = TileSize/2
        end
    end
    self.gridColon:draw(gridTenOffsetX - TileSize*(self.gridTen.thickness*2), gridTenOffsetY + colonOffset)
    self.gridTen:draw(gridTenOffsetX, gridTenOffsetY)
    self.gridOne:draw(gridTenOffsetX + TileSize*(self.gridTen.thickness+self.gridTen.width), gridTenOffsetY)

    -- draw objects
    for x = 1, self.gridTen.width do
        for y = 1, self.gridTen.height do
            if self.objects[x][y] == true then
                if self.gridTen.tiles[x][y] == true then
                    love.graphics.setColor(unpack(self.config.dotColor))
                else
                    local r = math.min(self.config.dotColor[1] + 0.3, 1)
                    local g = math.min(self.config.dotColor[2] + 0.3, 1)
                    local b = math.min(self.config.dotColor[3] + 0.3, 1)
                    love.graphics.setColor(r, g, b, 0.9)
                end
                love.graphics.circle(
                    "fill", gridTenOffsetX+(TileSize*(x-0.5)), gridTenOffsetY+(TileSize*(y-0.5)), TileSize * 0.2
                )
            end
        end
    end

    -- draw enemies
    for i = 1, #self.enemies do
        local opacity = 1
        if not self.gridTen:isWalkable(self.enemies[i].x, self.enemies[i].y) then
            opacity = 0.5
        end
        self.enemies[i]:draw(gridTenOffsetX, gridTenOffsetY, opacity)
    end

    -- draw powerups
    for i = 1, #self.powerups do
        local opacity = self.gridTen:isWalkable(self.powerups[i].x, self.powerups[i].y) and 1 or 0.5
        self.powerups[i]:draw(gridTenOffsetX, gridTenOffsetY, opacity)
    end

    if not self.lost then
        self.player:draw(gridTenOffsetX, gridTenOffsetY, 1)
    else
        -- player gets transparent when lost
        self.player:draw(gridTenOffsetX, gridTenOffsetY, 0.3)
    end

    love.graphics.setFont(fonts.default)
    love.graphics.setColor(1,1,1)
    if self.preWon == true and self.won == false then
        DimRect()
        if self.preOutro ~= nil then
            love.graphics.printf(self.preOutro, CANVAS_WIDTH * 0.1, CANVAS_HEIGHT/3, CANVAS_WIDTH * 0.8, "center")
        end
    elseif self.won == true then
        DimRect()
        if self.preOutro ~= nil then
            love.graphics.printf(self.preOutro, CANVAS_WIDTH * 0.1, CANVAS_HEIGHT/3, CANVAS_WIDTH * 0.8, "center")
        end
        love.graphics.printf(self.outro, CANVAS_WIDTH * 0.1, CANVAS_HEIGHT/2, CANVAS_WIDTH * 0.8, "center")
    elseif self.lost == true then
        DimRect()
        love.graphics.printf(self.lostMessage, CANVAS_WIDTH * 0.1, CANVAS_HEIGHT/2, CANVAS_WIDTH * 0.8, "center")
    elseif (self.started == false and self.intro ~= nil) then
        DimRect()
        love.graphics.printf(self.intro, CANVAS_WIDTH * 0.1, CANVAS_HEIGHT/3, CANVAS_WIDTH * 0.8, "center")
    else
        -- love.graphics.printf(self.content, 0, CANVAS_HEIGHT/2, CANVAS_WIDTH, "center")
    end

end

function Level:start()
    self.started = true
end

function Level:isWon()
    return self.won
end

function Level:win()
    if not self.preWon then
        self.preWon = true
        sounds.snooze:play()
    end
end

function Level:isLost()
    return self.lost
end

function Level:reset()
    if self.config.playerCharacter == "pacman" then
        self.player = Player:new(1, 1, self.config.playerColor)
    else
        self.player = Snake:new(1, 1)
    end

    self.won = false
    self.lost = false
    self.preWon = false

    self.gridTen:setDigit(self.config.startTime/10)
    self.gridTen:setWalls() -- reset walls
    self.gridOne:setDigit(self.config.startTime%10)
    self.gridColon:setColon(true)

    self.seconds = self.config.startTime%10
    self.tenSeconds = self.config.startTime/10

    self:fillWithObjects()
    self.remaining_objects = self:countObjects()

    self.player.x = 1
    self.player.y = 1

    self.player.powerupRemaining = 0
    self.preWonTimer = self.preWonTime

    -- init emenies
    self.enemies = {}
    for i = 1, self.config.enemies do
        local x, y = self.gridTen:getRandomFreePosition()
        while x == self.player.x and y == self.player.y do
            -- try again if it would be where the player is
            x, y = self.gridTen:getRandomFreePosition()
        end
        self.enemies[i] = Enemy:new(x, y, self.config.enemyColor)
    end

    -- init powerups
    self.powerups = {} -- remove powerups of previous try
    for i = 1, 1 do
        self:spawnPowerup()
    end
end

function Level:spawnPowerup()
    local x, y = self.gridTen:getRandomFreePosition()
    table.insert(self.powerups, Powerup:new(x, y))
end

-- go to next level if there is one, return false if not:
function nextLevel()
    if LevelManager.current < LevelManager.level_count then
        -- got to next level
        LevelManager.current = LevelManager.current + 1
        -- reset previous level to not lost, not won (and not started)
        Levels[LevelManager.current - 1]:reset()
        if Levels[LevelManager.current - 1].intro ~= nil then
            Levels[LevelManager.current - 1].started = false
        end

        return true
    else
        return false
    end
end

-- initialize levels list:
Levels = {}

-- this must be called once after the levels have been loaded from files:
function InitLevelManager()
    LevelManager = {
        current = 1, 
---@diagnostic disable-next-line: undefined-field
        level_count = table.getn(Levels),
        currentLevel = function ()
            return Levels[LevelManager.current]
        end,
    }
end

return Level
